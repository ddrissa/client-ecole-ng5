import { TestBed, inject } from '@angular/core/testing';

import { MatiereDetailResolveService } from './matiere-detail-resolve.service';

describe('MatiereDetailResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MatiereDetailResolveService]
    });
  });

  it('should ...', inject([MatiereDetailResolveService], (service: MatiereDetailResolveService) => {
    expect(service).toBeTruthy();
  }));
});
