import {Iadresse} from './interface/personne/iadresse';

export class Adresse implements Iadresse {


  constructor(public quartier?: string,
              public codePostal?: string,
              public email?: string,
              public mobile?: string,
              public bureau?: string,
              public tel?: string,) {

  }


}
