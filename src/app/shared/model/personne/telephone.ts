export class Telephone {

  constructor(public id?: number,
              public version?: number,
              public titre?: string,
              public numero?: string) {
  }
}
