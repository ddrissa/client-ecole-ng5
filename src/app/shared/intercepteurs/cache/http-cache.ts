import {HttpRequest, HttpResponse} from '@angular/common/http';

export  abstract class HttpCache {

  // TODO: ramener une du cache ou null au cas echeant
  abstract get(req: HttpRequest<any>): HttpResponse<any> | null;


  // TODO:Ajoute ou met à jour la réponse dans le cache.
  abstract put(req: HttpRequest<any>, rep: HttpResponse<any>): void;
}
