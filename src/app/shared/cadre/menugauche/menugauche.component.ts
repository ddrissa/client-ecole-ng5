import {Component, OnInit} from '@angular/core';
import {MenuItem} from 'primeng/primeng';

@Component({
  selector: 'app-menugauche',
  templateUrl: './menugauche.component.html',
  styleUrls: ['./menugauche.component.scss']
})
export class MenugaucheComponent implements OnInit {
   items: MenuItem[];

  constructor() {
  }

  ngOnInit() {
    this.items = [{
      label: 'ACCURIL',
      icon: 'fa fa-home',
      items: [
        {label: 'Accueil', icon: 'fa fa-life-ring', routerLink: ['/accueil']},
        {label: 'Invités', icon: 'fa fa-address-card', routerLink: ['/invite']}

      ]
    },
      {
        label: 'Formations',
        icon: 'fa fa-globe',
        items: [
          {label: 'Etudiant', icon: 'fa fa-address-book', routerLink: ['/etudiant']},
          {label: 'Matière', icon: 'fa fa-book', routerLink: ['/matiere']},
          {label: 'Cours', icon: 'fa fa-book', routerLink: ['/matiere']},
          {label: 'Sale', icon: 'fa fa-book', routerLink: ['/matiere']},
          {label: 'Promotion', icon: 'fa fa-book', routerLink: ['/matiere']},
          {label: 'Filière', icon: 'fa fa-book', routerLink: ['/matiere']}
        ]
      },
      {
        label: 'Administration',
        icon: 'fa fa-user-circle-o',
        items: [
          {label: 'Enseignant', icon: 'fa fa-address-card', routerLink: ['/enseignant']},
          {label: 'Employés', icon: 'fa fa-user-circle-o', routerLink: ['/employe']},
          {label: 'Administrateur', icon: 'fa fa-user-circle', routerLink: ['/admin']}
        ]
      }

    ];
  }

}
