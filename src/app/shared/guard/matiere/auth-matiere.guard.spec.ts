import { TestBed, async, inject } from '@angular/core/testing';

import { AuthMatiereGuard } from './auth-matiere.guard';

describe('AuthMatiereGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthMatiereGuard]
    });
  });

  it('should ...', inject([AuthMatiereGuard], (guard: AuthMatiereGuard) => {
    expect(guard).toBeTruthy();
  }));
});
