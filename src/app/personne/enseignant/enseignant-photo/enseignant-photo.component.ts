import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {EnseignantService} from '../../../shared/service/personne/enseignant/enseignant.service';
import {Enseignant} from '../../../shared/model/personne/enseignant';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpErrorResponse, HttpEvent, HttpEventType, HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-enseignant-photo',
  templateUrl: './enseignant-photo.component.html',
  styleUrls: ['./enseignant-photo.component.scss']
})
export class EnseignantPhotoComponent implements OnInit {
  enseignant: Enseignant;
  succesErreur: string;
  erreurMessage: string;
  photogroup: FormGroup;
  progression: number;
  imageEnseignantFile: File;
  @ViewChild('imageEns') ens_image;

  constructor(private  enseignantService: EnseignantService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.enseignantService.getEnseignatById(+params.get('id')))
      .subscribe(res => {
        if (res.status === 0) {
          this.enseignant = res.body;
          this.initForm();
        } else {
          this.succesErreur = res.messages.toString();
        }
      });
  }


  onSubmit() {
    const image = this.ens_image.nativeElement;
    if (image.files && image.files[0]) {
      this.imageEnseignantFile = image.files[0];
    }
    const imageFile = this.imageEnseignantFile;
    // console.log(imageFile);

    this.enseignantService.ajoutPhoto(imageFile, this.enseignant.numCni)
      .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            const percentDone = Math.round(100 * event.loaded / event.total);
            this.progression = percentDone;
            console.log(`Fichier est ${percentDone}% uploaded.`);
          } else if (event instanceof HttpResponse) {

            console.log('Apres ajout de photo', event.body);
            console.log('Fichier est completement uploader!');
          }
        },
        (err: HttpErrorResponse) => {
          if (err instanceof Error) {
            // Erreur cote client  ou de reseau
            this.erreurMessage = `Une erreur est servenue ${err.error.message}`;
            console.log('Une erreur est servenue', err.error.message);
          } else {
            // Erreur cote serveur ou backend
            this.erreurMessage = `Backend code retourner: ${err.status}, le contenu; ${err.error}`;
            console.log(`Backend code retourner: ${err.status}, le contenu: ${err.error}`);
          }
        });

    this.annuler();
  }

  annuler() {
    this.router.navigate(['enseignant/list']);
  }


  private initForm() {
    const cniEns = this.enseignant.numCni;
    this.photogroup = this.fb.group({
      cni: [cniEns],
      imageEns: ['', Validators.required]
    });
  }


}
