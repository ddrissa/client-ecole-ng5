import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MatButtonModule, MatButtonToggleModule, MatCardModule, MatChipsModule, MatExpansionModule, MatGridListModule,
  MatIconModule, MatInputModule, MatListModule,
  MatProgressBarModule, MatProgressSpinnerModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule, MatToolbarModule, MatTooltipModule
} from '@angular/material';

@NgModule({
  imports: [CommonModule,
    MatButtonModule, MatSidenavModule,
    MatCardModule, MatIconModule, MatToolbarModule,
    MatTooltipModule, MatGridListModule,
    MatProgressBarModule, MatSelectModule,
    MatChipsModule, MatInputModule, MatExpansionModule,
    MatSnackBarModule, MatButtonToggleModule, MatProgressSpinnerModule
  ],
  exports: [MatButtonModule, MatSidenavModule,
    MatCardModule, MatIconModule, MatToolbarModule,
    MatTooltipModule, MatGridListModule, MatListModule,
    MatProgressBarModule, MatSelectModule,
    MatChipsModule, MatInputModule, MatExpansionModule,
    MatSnackBarModule, MatButtonToggleModule, MatProgressSpinnerModule],
  declarations: []
})
export class MaterialModule {
}
