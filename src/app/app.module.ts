import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MaterialModule} from './/material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PrimengModule} from './/primeng.module';
import {AccueilComponent} from './shared/cadre/accueil/accueil.component';
import {MenugaucheComponent} from './shared/cadre/menugauche/menugauche.component';
import {NavbarComponent} from './shared/cadre/navbar/navbar.component';
import {FootbarComponent} from './shared/cadre/footbar/footbar.component';
import {InviteService} from './shared/service/personne/invites/invite.service';
import {InviteComponent} from './personne/invite/invite.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ConfirmationService} from 'primeng/primeng';
import {FormsModule} from '@angular/forms';
import {MessageErreurService} from './shared/service/messageErreur.service';
import {MatiereModule} from './matiere/matiere.module';
import {DureInterceptor} from './shared/intercepteurs/dure-interceptor';
import {EnseignantModule} from './personne/enseignant/enseignant.module';
import {CachingInterceptor} from './shared/intercepteurs/caching-interceptor';
import {CacheService} from './shared/intercepteurs/cache/cache.service';


@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    MenugaucheComponent,
    NavbarComponent,
    FootbarComponent,
    InviteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    PrimengModule,
    MatiereModule,
    EnseignantModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: DureInterceptor, multi: true},
{provide: HTTP_INTERCEPTORS, useClass: CachingInterceptor, multi: true},
InviteService, ConfirmationService, MessageErreurService, CacheService],
bootstrap: [AppComponent]
})
export class AppModule {
}
