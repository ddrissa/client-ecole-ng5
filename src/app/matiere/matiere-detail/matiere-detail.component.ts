import {Component, OnInit} from '@angular/core';
import {MatiereService} from '../../shared/service/matiere/matiere.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Matiere} from '../../shared/model/matiere/matiere';
import {Resultat} from '../../shared/model/resultat';

@Component({
  selector: 'app-matiere-detail',
  templateUrl: './matiere-detail.component.html',
  styleUrls: ['./matiere-detail.component.scss']
})
export class MatiereDetailComponent implements OnInit {
  matiere: Matiere = new Matiere();
  succesErreur: string;
  selectedMatiere: Matiere;

  constructor(private matiereService: MatiereService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.route.data.subscribe((data: { resultat: Resultat<Matiere> }) => {
      this.matiere = data.resultat.body;
      if (data.resultat.status !== 0) {
        this.closeMessage();
        this.succesErreur = data.resultat.messages.toString();
      }

    });

    //  this.matiereService.matiereSelected$.subscribe(mat => this.selectedMatiere = mat);
    /* this.route.params.switchMap((params: Params) => this.matiereService.getMatiere(+params['id']))
       .subscribe(res => {
         this.matiere = res.body;
       });*/
  }

  editer(m: Matiere) {
    this.router.navigate(['matiere/list/edit', m.id]);
    // Pour avoir matiere selectionner dans edit.
    // this.matiereService.matiereSelectStream(m);
  }

  supprimeMat(m: Matiere) {
    this.matiereService.supprimeerMatiere(m.id)
      .subscribe(res => console.log(res.messages.toString(), 'a été supprimé'));
    this.matiereService.matiereSupprimer(m);
    this.matiereService.filtreText(m.libelle);
    this.router.navigate(['matiere/list']);
  }


  matiereSupprimeStream(m: Matiere) {
    this.matiereService.matiereSupprimer(m);
  }

  private closeMessage() {
    setTimeout(() => {
      this.succesErreur = '';

    }, 7000);
  }


}
