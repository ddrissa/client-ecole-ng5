import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MatiereRoutingModule} from './matiere-routing.module';
import {MatiereService} from '../shared/service/matiere/matiere.service';
import {MatiereComponent} from './matiere.component';
import {MatiereCreateComponent} from './matiere-create/matiere-create.component';
import {MatiereDetailComponent} from './matiere-detail/matiere-detail.component';
import {MatiereEditComponent} from './matiere-edit/matiere-edit.component';
import {MatiereInviteComponent} from './matiere-invite/matiere-invite.component';
import {MatiereManageComponent} from './matiere-manage/matiere-manage.component';
import {MatieresListComponent} from './matieres-list/matieres-list.component';
import {MatiereNotFindComponent} from './matiere-not-find/matiere-not-find.component';
import {FormsModule} from '@angular/forms';
import {MatiereDetailResolveService} from '../shared/service/matiere/resolve/matiere-detail-resolve.service';
import {HttpClientModule} from '@angular/common/http';
import {MaterialModule} from '../material.module';
import {PrimengModule} from '../primeng.module';
import {AuthMatiereGuard} from '../shared/guard/matiere/auth-matiere.guard';
import {CanDesactiveMatiereGuard} from '../shared/guard/matiere/can-desactive-matiere.guard';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    PrimengModule,
    MatiereRoutingModule
  ],
  declarations: [
    MatiereComponent, MatiereCreateComponent, MatiereDetailComponent, MatiereEditComponent,
    MatiereInviteComponent, MatiereManageComponent, MatieresListComponent, MatiereNotFindComponent
  ],
  providers: [
    MatiereService,  AuthMatiereGuard, CanDesactiveMatiereGuard, MatiereDetailResolveService
  ]
})
export class MatiereModule {
}
