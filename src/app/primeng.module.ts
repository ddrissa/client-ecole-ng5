import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  ButtonModule,
  CheckboxModule,
  ConfirmDialogModule,
  ContextMenuModule,
  DataTableModule,
  DialogModule,
  DropdownModule,
  GrowlModule,
  InputMaskModule,
  InputTextModule,
  MessagesModule, OrderListModule,
  OverlayPanelModule,
  PanelMenuModule,
  PanelModule,
  SharedModule
} from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule, PanelModule, PanelMenuModule, DataTableModule, DropdownModule, InputMaskModule, MessagesModule,
    SharedModule, DialogModule, GrowlModule, CheckboxModule, OverlayPanelModule, ConfirmDialogModule, ContextMenuModule,
    OrderListModule

  ],

  exports: [
    ButtonModule, PanelModule, PanelMenuModule, DataTableModule, DropdownModule, InputMaskModule, MessagesModule,
    SharedModule, DialogModule, GrowlModule, CheckboxModule, OverlayPanelModule, ConfirmDialogModule, ContextMenuModule,
    InputMaskModule, InputTextModule, OrderListModule
  ],
  declarations: []
})
export class PrimengModule {
}
