import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AccueilComponent} from './shared/cadre/accueil/accueil.component';
import {InviteComponent} from './personne/invite/invite.component';

const routes: Routes = [
  {
    path: '', redirectTo: '/accueil', pathMatch: 'full'

  },
  {
    path: 'accueil', component: AccueilComponent
  },
  {
    path: 'invite', component: InviteComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
